{-# LANGUAGE BangPatterns #-}
module Brain where

import Control.Lens
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Writer
import Data.Ord
import Data.Maybe
import Data.List
import Data.Monoid
import qualified Data.Map.Strict as Map
import Debug.Trace

import GameInitModel
import CarPositionsModel

import qualified CarPositionsModel as CPM
import qualified GameInitModel as GIM

type Tick = Int
type LaneNumber = Int
type Speed = Float
type Angle = Float
type TrackSegment = [(Int, Piece)]

data Brain = Brain { bTrack :: Track
                   , bSwitchPending :: Bool
                   , bSwitchingLane :: Bool
                   , bMyPosition :: PiecePosition
                   , bMyCarPosition :: CarPosition
                   , bBrakingDistances :: [Float]
                   , bCurrentThrottle :: Float
                   , bCurrentSpeed :: Float
                   , bPieceKnowledge :: [PieceKnowledge]
                   , bCars :: CarDatas
                   , bCarPieceData :: Map.Map String [PieceData]
                   , bBrakingPower :: BrakingPowerFunc
                   , bBrakingData :: Map.Map Float Float
                   , bRegulateIntegral :: Float
                   , bRegulateError :: Float
                   , bTurboAvailable :: Bool
                   , bTurboFactor :: Float
                   , bTurboOn :: Bool
                   , bTick :: Tick
                   , bDrag :: Maybe Float
                   , bMass :: Maybe Float
                   }

drag :: Brain -> Float
drag = fromMaybe 0.02 . bDrag

mass :: Brain -> Float
mass = fromMaybe 0.02 . bMass

otherCars :: Brain -> CarDatas
otherCars b = Map.filterWithKey (\k _ -> k /= myName b) $ bCars b

myName :: Brain -> String
myName = carIdName . CPM.carId . bMyCarPosition

adjustThrottleForTurbo :: Brain -> Float -> Float
adjustThrottleForTurbo b t | bTurboOn b == False = t
                           | otherwise           = t / (bTurboFactor b)

segmentsBy :: Brain -> ((Int, Piece) -> Bool) -> [TrackSegment]
segmentsBy b f = filter (not . null) $ foldl (\segs p -> if f p then (init segs) ++ [last segs ++ [p]] else segs ++ [[]]) [[]] $ zip [0..] (pieces $ bTrack b)

segmentKnowledge :: Brain -> TrackSegment -> [PieceKnowledge]
segmentKnowledge b s = take (max - min) $ drop (minimum indices) (bPieceKnowledge b)
    where
        indices = map fst s
        min = minimum indices
        max = maximum indices

data PieceKnowledge = PieceKnowledge
                        { speed :: Speed
                        , maxSpeed :: Speed
                        , crashed :: Bool
                        , pkDifficulty :: Float
                        }
                        deriving (Show, Read)

defaultKnowledge :: Track -> (Int, Piece) -> PieceKnowledge
defaultKnowledge t (i, p) | not $ isBend p  = PieceKnowledge 1 1 False 0
                          | otherwise       = PieceKnowledge (calcSegSpeed' seg) 1 False (calculateBendDifficulty p)
    where
        ps = pieces t
        seg = drop i $ circularize ps

pieceCrashed :: Speed -> PieceKnowledge -> PieceKnowledge
pieceCrashed s p = p { maxSpeed = min s ((maxSpeed p) - 0.01), speed = clampThrottle newSpeed, crashed = True }
    where
        newSpeed = (speed p) - ((speed p) / 4)

piecePassed :: Speed -> PieceKnowledge -> PieceKnowledge
piecePassed actualSpeed p | crashed p = p { crashed = False }
                          | pkDifficulty p == 0 = p
                          | otherwise = if (min actualSpeed (speed p)) <= (maxSpeed p)
                                          then p { speed = min (clampThrottle (actualSpeed + 0.01)) (maxSpeed p) }
                                          else p

knownSpeed :: Int -> Brain -> Speed
knownSpeed pieceIdx b = speed ((bPieceKnowledge b) !! pieceIdx)

nextSlowPoint :: Int -> Brain -> Int
nextSlowPoint pieceIdx b = maybe pieceIdx fst $ find (\(i,pk) -> (speed pk) < (bCurrentSpeed b)) $ drop (pieceIdx + 1) circularPK
    where
        pk = bPieceKnowledge b
        circularPK = circularize pk

nextPiece :: (Monad a) => BotM a Piece
nextPiece = get >>= (\b -> return ((cycle $ pieces $ bTrack b) !! ((pieceIndex $ bMyPosition b) + 1)))

clampThrottle :: Float -> Speed
clampThrottle = clamp 0 1

clampSpeed :: Float -> Speed
clampSpeed = max 0

type CarDatas = Map.Map String CarData

data CarData = CarData { cdSpeed :: Speed
                       , cdPrevSpeed :: Speed
                       , cdAngleSpeed :: Speed
                       , cdCrashed :: Bool
                       , cdAngle :: Float
                       , cdPosition :: PiecePosition
                       , cdCrashTick :: Tick
                       , cdDNF :: Bool
                       }
                       deriving (Show, Read)

isOnTrack :: CarData -> Bool
isOnTrack cd = not (cdDNF cd) && not (cdCrashed cd)

initCarData :: [Car] -> CarDatas
initCarData = Map.fromList . map toData
    where
        toData (Car { GameInitModel.carId = c }) = (carIdName c, CarData 0 0 0 False 0 (PiecePosition 0 0 (CarLane 0 0) 0) 0 False)

initCarPieceData :: [Car] -> Map.Map String [PieceData]
initCarPieceData cs = Map.fromList $ map conv cs
    where
        conv (Car { GIM.carId = cid }) = (carIdName cid, [])

leaderboard :: CarDatas -> [String]
leaderboard cds = map fst $ sortBy rank $ Map.toList cds
    where
        rank (_, a) (_, b) = comparing pi a b `mappend` comparing pd a b
        pi = pieceIndex . cdPosition
        pd = inPieceDistance . cdPosition

enteredBend :: Track -> CarData -> CarData -> Bool
enteredBend t old new = (not $ isBend op) && (isBend np)
    where
        ps = pieces t
        op = ps !! (pieceIndex $ cdPosition old)
        np = ps !! (pieceIndex $ cdPosition new)

completedBend :: Track -> CarData -> CarData -> Bool
completedBend t old new = (isBend op) && (not $ isBend np)
    where
        ps = pieces t
        op = ps !! (pieceIndex $ cdPosition old)
        np = ps !! (pieceIndex $ cdPosition new)

updateCarData :: Track -> CarPosition -> CarData -> CarData
updateCarData t cp cd = cd { cdAngle = CPM.angle cp, cdPosition = pp, cdSpeed = newSpeed, cdPrevSpeed = cdSpeed cd, cdAngleSpeed = newAngleSpeed }
    where
        pp = piecePosition cp
        newSpeed = calculateSpeed ((pieces t) !! (pieceIndex $ cdPosition cd)) (cdPosition cd) pp ((lanes t) !! (endLaneIndex $ lane pp))
        newAngleSpeed = (cdAngle cd) - (CPM.angle cp)

calculateSpeed :: Piece -> PiecePosition -> PiecePosition -> Lane -> Speed
calculateSpeed oldPiece oldPos newPos newLane = if positionDiff >= 0
                                                  then positionDiff / 10
                                                  else (positionDiff + (pieceLen (distanceFromCenter newLane) oldPiece)) / 10
    where
        positionDiff = (inPieceDistance newPos) - (inPieceDistance oldPos)

type LapData = [(Int, CarPosition)]

data PieceData = PieceData { pdPosition :: CarPosition
                           , pdSpeed :: Speed
                           }
                           deriving (Show, Read)

pdPiecePosition :: PieceData -> PiecePosition
pdPiecePosition = piecePosition . pdPosition

pdPieceIndex :: PieceData -> Int
pdPieceIndex = pieceIndex . pdPiecePosition


newPieceData :: PiecePosition -> CarPosition -> Piece -> Lane -> PieceData
newPieceData old newCp p l = PieceData { pdPosition = newCp
                                       , pdSpeed = calculateSpeed p old (piecePosition newCp) l
                                       }

updateCarPieceData :: [PieceData] -> PieceData -> [PieceData]
updateCarPieceData pds pd | length pds == 0 = [pd]
                          | otherwise       = if pdPieceIndex (last pds) /= pdPieceIndex pd
                                                then [pd]
                                                else pds ++ [pd]

data PlayMode = Create | Join | DefaultJoin deriving (Show, Read, Eq)
data BotConfig = BotConfig { bcName :: String, bcTrackName :: String, bcPassword :: Maybe String, bcMode :: PlayMode, bcNumCars :: Int }

type BotM a = StateT Brain (ReaderT BotConfig a)

runBotM :: BotM IO a -> BotConfig -> IO ()
runBotM f c = void $ runReaderT (runStateT f undefined) c

data ActionChoice = SwitchChoice Side
                  | BrakeChoice
                  | TurboChoice
                  | AccelerateChoice Speed
                  | PingChoice
                  | NoChoice
                  deriving (Show, Read, Eq, Ord)

noChoice :: (Monad m) => BotM m ActionChoice
noChoice = return NoChoice

choose :: (Monad m) => ActionChoice -> BotM m ActionChoice
choose = return

segmentLength :: Int -> TrackSegment -> Float
segmentLength offset segment = sum $ map (pieceLen offset . snd) segment

data BendDirection = BendLeft | BendRight deriving (Show, Read, Eq)

data Side = Left | Right deriving (Show, Read, Eq, Ord)

laneSwitchSide :: Lane -> Lane -> Maybe Side
laneSwitchSide from to = if fromDist == toDist
                           then Nothing
                           else if fromDist > toDist
                                    then Just Brain.Left
                                    else Just Brain.Right
    where
        fromDist = distanceFromCenter from
        toDist = distanceFromCenter to

laneAfterSwitch :: Track -> Side -> LaneNumber -> LaneNumber
laneAfterSwitch t s current = clamp 0 maxLane (change current)
    where
        maxLane = (length $ lanes t) - 1
        change = case s of
                   Brain.Left  -> (-) 1
                   Brain.Right -> (+) 1

slowCarOnLane :: Brain -> PiecePosition -> Float -> Lane -> Bool
slowCarOnLane b pp distance l = any (\cd -> distanceToCar pp (cdPosition cd) l ps < distance) slower
    where
        slower = filter (\cd -> isOnTrack cd && isSlower cd) $ map snd $ Map.toList $ otherCars b
        isSlower cd = let s = cdSpeed cd + 0.3 in s < bCurrentSpeed b && s < knownSpeed (pieceIndex $ cdPosition cd) b
        ps = pieces $ bTrack b

-- | Check if any obstacles exist on lane from position.
-- Currently only considers crashed cars as obstacles
obstacleOnLane :: Brain -> PiecePosition -> Lane -> Bool
obstacleOnLane b pp l = isJust $ find (\cd -> let s = seg pp $ cdPosition cd
                                                  ticks = ticksForSegment s (segmentKnowledge b s) pp (bCurrentSpeed b)
                                              in trace ("ticks: " ++ (show $ ticks)) $ sameLane cd && ticks > 0 && ticks < (crashRemaining cd) + 20) crashed
    where
        crashed = filter cdCrashed $ map snd $ Map.toList cds
        t = bTrack b
        cds = otherCars b
        sameLane cd = (GIM.index l) == (endLaneIndex $ lane $ cdPosition cd)
        seg s e = getSegment t (pieceIndex s) (pieceIndex e)
        crashRemaining cd = crashDuration - ((bTick b) - (cdCrashTick cd))

obstacleOnLane' :: Brain -> PiecePosition -> Lane -> Bool
obstacleOnLane' b pp l = isJust $ find (\cd -> let ticks cd = ticksForDistance (bCurrentSpeed b) (distanceToCar pp (cdPosition cd) l (pieces t)) in trace ("ticks: " ++ (show $ ticks cd)) $ sameLane cd && ticks cd < crashDuration + 20) crashed
    where
        crashed = filter cdCrashed $ map snd $ Map.toList cds
        t = bTrack b
        cds = otherCars b
        sameLane cd = (GIM.index l) == (endLaneIndex $ lane $ cdPosition cd)

getSegment :: Track -> Int -> Int -> TrackSegment
getSegment t start end = zip [start..end] $ take (end - start) $ drop start ps
    where
        ps = pieces t

-- | Estimate numbred of ticks it takes to travel certain segment of track in optimal circumstances
-- Not 100% accurate but close enough
ticksForSegment :: TrackSegment -> [PieceKnowledge] -> PiecePosition -> Speed -> Tick
ticksForSegment seg pks startPoint startSpeed = length $ takeWhile (\(_, d, _, _) -> d > 0) $ iterate sim (startSpeed, totalDistance, map (\p -> (0, pieceLen 0 $ snd p)) seg, pks)
    where
        totalDistance = (sum $ map (pieceLen 0 . snd) seg) - (inPieceDistance startPoint)
        adjustSpeed s pk = if s <= (speed pk) then s + (defaultAccelPowerFunc s) else s - (defaultBrakingPowerFunc s)
        travel s = s * 10
        sim (s, d, ps, pks) = let dist = travel s
                                  (pt, pl) = (fst p + dist, snd p)
                                  next     = pt >= pl
                                  p = head ps
                                  ps' = if length ps == 1 then [p] else tail ps
                                  pk = head pks
                                  pks' = if length pks == 1 then [pk] else tail pks
                              in (adjustSpeed s pk, d - dist, if next then ps' else (pt,pl):ps', if next then pks' else pk:pks')

switchToAvoidSlow :: Brain -> Maybe Side
switchToAvoidSlow b = if not $ slowCarOnLane b pp 50 myLane
                        then Nothing
                        else mSwitch >>= (\s -> bestSwitchDir b s `mplus` (availableLane >>= laneSwitchSide myLane))
    where
        pp = bMyPosition b
        myLane = (lanes $ bTrack b) !! (endLaneIndex $ lane pp)
        mSwitch = nextSwitchIndex $ drop (pieceIndex pp) (pieces $ bTrack b)
        availableLane = find (not . slowCarOnLane b pp 50) $ possibleLanes b

possibleLanes :: Brain -> [Lane]
possibleLanes b = map getLane $ filter (\n -> n >= 0 && n < laneCount) [currentLane + 1, currentLane - 1]
    where
        laneCount = length $ lanes $ bTrack b
        currentLane = endLaneIndex $ lane $ bMyPosition b
        getLane i = (lanes $ bTrack b) !! i

bestSwitchDir :: Brain -> Int -> Maybe Side
bestSwitchDir b switchIdx = switchDir >>= (\s -> if shouldSwitch s then Just s else Nothing)
    where
        circularTrack = circularize (pieces $ bTrack b)
        (nextSwitchIdx, _) = fromJust $ find (isSwitch . snd) $ drop (switchIdx + 1) circularTrack
        bends = map snd $ filter (\(i, (_, p)) -> (i < nextSwitchIdx) && (isBend p)) $ drop (switchIdx + 1) $ zip [0..] circularTrack
        (lefts, rights) = partition (\(_, p) -> (bendDirection p) == BendLeft) bends
        switchDir = if (length lefts) == (length rights)
                      then Nothing
                      else Just (if (length lefts) > (length rights) then Brain.Left else Brain.Right)
        myLane = endLaneIndex $ lane $ bMyPosition b
        shouldSwitch Brain.Left = (myLane - 1) >= 0
        shouldSwitch Brain.Right = (myLane + 1) < (length $ lanes $ bTrack b)

needsSwitch :: Brain -> Maybe Side
needsSwitch b = if (bSwitchPending b) || (bSwitchingLane b) || (not $ fromMaybe False $ liftM2 (\a b -> (fst a) <= (fst b)) mSwitch mBend)
                  then trace "No switch" Nothing
                  else
                      let (switchIdx, _) = fromJust mSwitch
                      in bestSwitchDir b switchIdx
    where
        circularTrack = concat $ replicate 2 $ zip [0..] (pieces $ bTrack b)
        mSwitch = find (isSwitch . snd) $ drop (pieceIndex $ bMyPosition b) $ circularTrack
        mBend = mSwitch >>= (\(i, _) -> find (isBend . snd) $ drop (i + 1) $ circularTrack)

nextSwitchIndex :: [Piece] -> Maybe Int
nextSwitchIndex = findIndex isSwitch

nextBendIndex :: [Piece] -> Int -> Maybe Int
nextBendIndex ps start = fmap fst $ find (isBend . snd) $ drop start $ circularize ps

circularize :: [a] -> [(Int, a)]
circularize = concat . replicate 2 . zip [0..]

bendDirection :: Piece -> BendDirection
bendDirection p = case pieceAngle p of
                    Nothing -> BendLeft
                    Just a  -> if a < 0 then BendLeft else BendRight

isSwitch :: Piece -> Bool
isSwitch = fromMaybe False . switch

isBend :: Piece -> Bool
isBend = isJust . radius

isStraight :: Piece -> Bool
isStraight p = (not $ isSwitch p) && (not $ isBend p)

clamp :: (Num n, Ord n) => n -> n -> n -> n
clamp a b = min b . max a

distanceTo' :: PiecePosition -> Lane -> Int -> [Piece] -> Float
distanceTo' pos lane targetIndex pieces = firstLen + (sum $ map (pieceLen offset . snd) $ takeWhile (\(i, _) -> i /= targetIndex) $ drop (1 + (pieceIndex pos)) cps)
    where
        cps = circularize pieces
        offset = distanceFromCenter lane
        firstLen = (pieceLen offset $ head pieces) - (inPieceDistance pos)

distanceToCar :: PiecePosition -> PiecePosition -> Lane -> [Piece] -> Float
distanceToCar src dst lane ps = (distanceTo' src lane (pieceIndex dst) ps) + (inPieceDistance dst)

-- | Return number of ticks at speed it would take to travel distance
ticksForDistance :: Speed -> Float -> Tick
ticksForDistance speed dist = ceiling (dist / (speed * 10))

calcSegSpeed' :: TrackSegment -> Speed
calcSegSpeed' seg = k / (turnAmount / angleAmount)
    where
        k = 0.063
        maxLen = 160 :: Float
        (turnAmount, angleAmount) = foldl sumAll (0, 0) $ takeWhile (\(d,_,_,_) -> d < maxLen) $ iterate calc (0, 0, 0, map snd seg)
        calc (d, ta, aa, p:ps) = let pLen = pieceLen 0 p
                                     takeL = maxLen - d
                                     takeL' = if takeL > pLen then pLen else takeL
                                     anglePerc = (abs $ fromMaybe 0 $ pieceAngle p) * takeL' / pLen
                                 in if not $ isBend p
                                      then (d + pLen, ta, aa, ps)
                                      else (d + pLen, ta + anglePerc / sqrt (fromIntegral $ fromMaybe 0 $ radius p), aa + anglePerc, ps)

        calc (_, ta, aa, []) = (maxLen, ta, aa, [])
        sumAll (t, a) (_, t', a', _) = (t + t', a + a')


calcSegSpeed :: TrackSegment -> Speed
calcSegSpeed seg = clampThrottle $ e / sumAngles
    where
        e = 0.9
        maxA = maximum $ map (abs . fromMaybe 0 . pieceAngle . snd) seg
        maxR = fromIntegral $ minimum $ map (fromMaybe 0 . radius . snd) seg
        sumAngles = sum $ map (\(_, p) -> let a = maxA
                                              r = maxR
                                          in (s a r) / r) seg
        s a r = a
        -- s a r = 2 * r * pi * a / 360

-- NOTE ASSUMES BENDS ARE NOT FINISH LINES (DOES NOT CIRCULARIZE TRACK)
fullBend :: Int -> [Piece] -> TrackSegment
fullBend target ps = if null bendsAfter then [] else concat [bendsBefore, bendsAfter]
    where
        targetDir = bendDirection (ps !! target)
        isValid p = ((bendDirection p) == targetDir) && (isBend p)
        (before, after) = splitAt target (zip [0..] ps)
        bendsAfter = takeWhile (isValid . snd) after
        bendsBefore = reverse $ takeWhile (isValid . snd) $ reverse before

inTrackSegment :: PiecePosition -> TrackSegment -> Bool
inTrackSegment pp ps = any (\(i, _) -> i == (pieceIndex pp)) ps

-- assumes all pieces are in sequence
traveledWithin :: TrackSegment -> PiecePosition -> Lane -> Float
traveledWithin ps pp lane = before + (inPieceDistance pp)
    where
        traveledPieces = takeWhile (\(i,_) -> i < (pieceIndex pp)) ps
        before = if null traveledPieces
                   then 0
                   else sum $ map (pieceLen (distanceFromCenter lane) . snd) $ traveledPieces

distanceToMidpoint :: PiecePosition -> Lane -> Int -> [Piece] -> Float
distanceToMidpoint pp lane targetIdx ps = if inTrackSegment pp targetBend
                                            then halfLength - (traveledWithin targetBend pp lane)
                                            else distToStart + halfLength
    where
        targetBend = fullBend targetIdx ps
        distToStart = distanceTo' pp lane targetIdx ps
        halfLength = (sum $ map ((pieceLen $ distanceFromCenter lane) . snd) $ targetBend) / 2

pieceLen :: Int -> Piece -> Float
pieceLen offset p = case (pieceLength p) of
               Just l  -> l
               Nothing -> abs ((fromIntegral (fixedOffset + (fromJust $ radius p))) * (deg2rad $ fromJust $ pieceAngle p))
    where
        fixedOffset = if (bendDirection p) == BendRight then offset * (-1) else offset

deg2rad :: Float -> Float
deg2rad d = d / 180 * pi

-- | As short corners are harder than long corners of same angle, 
-- this function tries to guess the difficulty based on that factor.
calculateBendDifficulty :: Piece -> Float
calculateBendDifficulty p = (pieceLen 0 p) * (abs $ fromJust $ pieceAngle p)

calculateBendSpeed :: Piece -> Float
calculateBendSpeed p | isBend p  = speed $ calculateBendDifficulty p
                     | otherwise = 1
    where
        speed d | d > 3000 = 0.3
                | otherwise = 0.65

calculateBrakeDistance :: Speed -> Speed -> Float
calculateBrakeDistance bendSpeed mySpeed = if speedDiff <= 0
                                                     then 0
                                                     else (speedDiff / (brakingPowerPerTick mySpeed)) * 10
    where speedDiff = mySpeed - bendSpeed

calculateBrakeDistance' :: Brain -> Speed -> Speed -> Float
calculateBrakeDistance' b tar cur = (mass b / drag b) * current * (1 - (exp ((0 - drag b) * ticks / mass b)))
    where
        ticks = (log (target / current)) * (mass b) / (0 - drag b)
        current = cur * 10
        target = tar * 10

brakingPowerPerTick :: Speed -> Float
brakingPowerPerTick _ = 0.015

type BrakingPowerFunc = Speed -> Float
defaultBrakingPowerFunc :: BrakingPowerFunc
defaultBrakingPowerFunc s = s * 0.02

defaultAccelPowerFunc :: Speed -> Float
defaultAccelPowerFunc s = s * 0.02

generateBrakingPowerFunction :: [(Speed, Float)] -> BrakingPowerFunc
generateBrakingPowerFunction d = (* factor)
    where
        (_, factor, _) = linearRegression d

centripetalForce :: Float -> Int -> Speed -> Float
centripetalForce mass radius speed = ((mass * (speed*speed)) / (fromIntegral radius))

-- stolen from hstats-0.3
linearRegression :: (Floating b) => [(b, b)] -> (b, b, b)
linearRegression xys = let !xs = map fst xys
                           !ys = map snd xys
                           !n = fromIntegral $ length xys
                           !sX = sum xs
                           !sY = sum ys
                           !sXX = sum $ map (^ 2) xs
                           !sXY = sum $ map (uncurry (*)) xys
                           !sYY = sum $ map (^ 2) ys
                           !alpha = (sY - beta * sX) / n
                           !beta = (n * sXY - sX * sY) / (n * sXX - sX * sX)
                           !r = (n * sXY - sX * sY) / (sqrt $ (n * sXX - sX^2) * (n * sYY - sY ^ 2))
             in (alpha, beta, r)

crashDuration :: Tick
crashDuration = 400

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)
import System.Directory (doesFileExist)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.Ord
import Data.List
import Data.Maybe
import Data.Map (adjust, fromList)
import qualified Data.Map as Map
import Control.Lens
import Control.Monad
import Control.Monad.Trans
import Control.Monad.State
import Control.Monad.Reader
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), (.:?), Result(..))
import Debug.Trace

import GameInitModel
import CarPositionsModel
import qualified CarPositionsModel as CPM

import Brain

type ClientMessage = String

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
createMessage botname botkey track pass num = "{\"msgType\":\"createRace\",\"data\":{\"botId\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"},\"trackName\":\"" ++ track ++ "\",\"password\":\"" ++ pass ++ "\",\"carCount\":" ++ num ++ "}}"
joinRaceMessage botname botkey track pass num = "{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"},\"trackName\":\"" ++ track ++ "\",\"password\":" ++ pass ++ ",\"carCount\":" ++ num ++"}}"
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
switchMessage side = "{\"msgType\":\"switchLane\",\"data\":\"" ++ (show side) ++ "\"}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"
turboMessage = "{\"msgType\":\"turbo\",\"data\":\"IT'S TURBO TIME!\"}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
    args <- getArgs
    case (splitAt 4 args) of
      ([server, port, botname, botkey], xs) -> do
        run server port botname botkey (arg 0 xs) (arg 1 xs) (arg 2 xs) (arg 3 xs)
      _ -> do
        putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey> [track] [mode] [password] [num bots]"
        exitFailure
    where
        arg n xs | n >= length xs = Nothing
                 | otherwise      = Just (xs !! n)


run :: String -> String -> String -> String -> Maybe String -> Maybe String -> Maybe String -> Maybe String -> IO ()
run server port botname botkey mtrack mmode mpass mnumbots = do
  let c = BotConfig botname (fromMaybe "keimola" mtrack) mpass (maybe Brain.DefaultJoin (\s -> if s == "join" then Brain.Join else Create) mmode) (maybe 1 read mnumbots)

  h <- connectToServer server port
  hSetBuffering h LineBuffering
  case (bcMode c) of
    Create -> hPutStrLn h $ createMessage botname botkey (bcTrackName c) (fromMaybe "" $ bcPassword c) (show $ bcNumCars c)
    Brain.Join  -> hPutStrLn h $ joinRaceMessage botname botkey (bcTrackName c) (maybe "null" (\p -> if p == "" then "null" else "\"" ++ p ++ "\"") $ bcPassword c) (show $ bcNumCars c)
    DefaultJoin -> hPutStrLn h $ joinMessage botname botkey

  runBotM (handleMessages h) c

handleMessages h = do
  msg <- liftIO $ hGetLine h
  -- liftIO $ print msg
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h serverMessage
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = Join
                   | GameInit GameInitData
                   | CarPositions [CarPosition] Int
                   | GameStart
                   | LapFinished
                   | Crash Tick CrashData
                   | Spawn Tick SpawnData
                   | GameEnd
                   | TurboAvailable TurboData
                   | TurboEnd CarId
                   | DNF DNFData
                   | Unknown String

handleServerMessage :: Handle -> ServerMessage -> BotM IO ()
handleServerMessage h serverMessage = do
  r <- respond serverMessage >>= handleChoice
  case r of
    Nothing -> handleMessages h
    Just s  -> (liftIO $ hPutStrLn h s) >> (handleMessages h)

handleChoice :: ActionChoice -> BotM IO (Maybe String)
handleChoice c = do
        b <- get
        let rs = leaderboard (bCars b)

        turboMsg <- if c /= NoChoice && bTurboAvailable b then handleTurbo else return Nothing

        if isJust turboMsg
          then return $ fmap choiceToMsg turboMsg
          else case c of
                   BrakeChoice ->
                       if carInFront b && bTurboOn b
                         then do
                             liftIO $ print "RAMMING SPEED"
                             return $ Just $ choiceToMsg $ AccelerateChoice 1
                         else
                            return $ Just $ choiceToMsg c

                   SwitchChoice side -> do
                       let newLane = laneAfterSwitch (bTrack b) side (endLaneIndex $ lane $ bMyPosition b)
                       liftIO $ print ("Testing for obstacle on lane " ++ (show newLane))
                       modify (\b -> b { bSwitchPending = True })
                       if slowCarOnLane b (bMyPosition b) 200 ((lanes $ bTrack b) !! newLane)
                         then trace "Slow car, no switch" $ return $ Just $ choiceToMsg NoChoice
                         else do
                             trace "Switch OK" $ return $ Just $ choiceToMsg c

                   otherwise   -> return (if c /= NoChoice then Just $ choiceToMsg c else Nothing)
    where
        choiceToMsg BrakeChoice = throttleMessage 0
        choiceToMsg (AccelerateChoice s) = throttleMessage s
        choiceToMsg (SwitchChoice s)     = switchMessage s
        choiceToMsg TurboChoice          = turboMessage
        choiceToMsg _    = pingMessage

handleTurbo :: BotM IO (Maybe ActionChoice)
handleTurbo = do
        b <- get
        let isLongestStraight = elem (pieceIndex $ bMyPosition b) $ map fst $ last $ sortBy (comparing $ segmentLength 0) $ segmentsBy b (not . isBend . snd)

        if carInFront b
          then do
              modify (\b -> b { bTurboAvailable = False, bTurboOn = True })
              liftIO $ print "IT'S TURBO RAM TIME"
              return Nothing
              return $ Just TurboChoice
          else return Nothing

carInFront :: Brain -> Bool
carInFront b = isJust $ find (\(_, cd) -> (isOnTrack cd) && bMyPosition b /= cdPosition cd && inPieceInFront (bMyPosition b) (cdPosition cd) b) $ Map.toList $ bCars b
    where
        inPieceInFront me them b = (endLaneIndex $ lane me) == (endLaneIndex $ lane them) && (distanceToCar me them ((lanes $ bTrack b) !! (endLaneIndex $ lane me)) (pieces $ bTrack b)) < 100

respond :: ServerMessage -> BotM IO ActionChoice
respond message = case message of
  Main.Join -> do
      liftIO $ putStrLn "Joined"
      noChoice
  GameInit gameInit -> do
      let trackName = name $ track $ race gameInit
      liftIO $ writeFile (trackName ++ ".trackdata") (show $ track $ race gameInit)

      liftIO $ putStrLn $ "GameInit: " ++ (reportGameInit gameInit)
      put $ Brain { bTrack = track $ race $ gameInit
                  , bSwitchPending = False
                  , bSwitchingLane = False
                  , bMyPosition = PiecePosition 0 0 undefined 0
                  , bMyCarPosition = undefined
                  , bBrakingDistances = map (\p -> if isStraight p then 0 else 50) $ piecesOfGame gameInit
                  , bCurrentThrottle = 1.0
                  , bCurrentSpeed = 0.0
                  , bPieceKnowledge = map (defaultKnowledge $ track $ race gameInit) $ zip [0..] $ pieces $ track $ race gameInit
                  , bCars = initCarData $ cars $ race gameInit
                  , bCarPieceData = initCarPieceData $ cars $ race gameInit
                  , bBrakingPower = defaultBrakingPowerFunc
                  , bBrakingData = fromList []
                  , bRegulateIntegral = 0
                  , bRegulateError = 0
                  , bTurboAvailable = False
                  , bTurboFactor = 1
                  , bTurboOn = False
                  , bTick = 0
                  , bDrag = Nothing
                  , bMass = Nothing
                  }

      noChoice
  LapFinished -> do
      noChoice
  Crash tick (CrashData name) -> do
      myName <- liftM bcName ask
      b <- get

      modify (\b ->
        let cd = fromJust $ Map.lookup name $ bCars b 
        in trace ("Updating because of crash: " ++ (show cd)) $ b { bPieceKnowledge = over (element $ pieceIndex $ cdPosition cd) (pieceCrashed $ cdSpeed cd) (bPieceKnowledge b) })
      modify (\b -> b { bCars = adjust (\c -> c { cdCrashed = True, cdCrashTick = tick }) name $ bCars b } )

      if name /= myName
        then do
            noChoice
        else do
          liftIO $ print "##########################################"
          liftIO $ print ("######### CRASH    Tick: " ++ (show tick) ++ " ########")
          liftIO $ print "##########################################"

          -- liftIO $ writeFile "brain.show" $ show b

          noChoice

  Spawn tick (SpawnData name) -> do
      modify (\b -> b { bCars = adjust (\c -> c { cdCrashed = False }) name $ bCars b } )
      noChoice

  DNF (DNFData (CarId { carIdName = n })) -> do
      modify (\b -> b { bCars = adjust (\c -> c { cdDNF = True }) n $ bCars b } )
      noChoice

  GameStart -> do
      liftIO $ print "Game start"
      modify (\b -> b { bSwitchPending = False })
      choose PingChoice
  GameEnd -> do
      liftIO $ print "Game end: Saving piece data"
      b <- get
      liftIO $ writeFile ((name $ bTrack b) ++ ".show") $ show $ bPieceKnowledge b
      noChoice
  TurboAvailable (TurboData factor)-> do
      modify (\b -> b { bTurboAvailable = True, bTurboFactor = factor })
      noChoice
  TurboEnd (CarId { carIdName = n }) -> do
      me <- liftM myName get
      when (n == me) $ modify (\b -> b { bTurboFactor = 1, bTurboOn = False })
      noChoice
  CarPositions carPositions tick -> do
      myName <- liftM bcName ask
      brain <- get
      let myPos = piecePosition $ fromJust $ findCar myName carPositions

      updateCars tick carPositions
      modify $ updateBrain myName carPositions

      brain' <- get
      let switchSide = switchToAvoidSlow brain' `mplus` needsSwitch brain'
      nextIsSwitch <- liftM isSwitch nextPiece

      if nextIsSwitch && isJust switchSide
        then switchLane (fromJust switchSide) tick
        else adjustThrottle myPos tick
  Unknown msg -> do
      liftIO $ putStrLn $ "Unknown message: " ++ msg
      noChoice

updateCars :: Tick -> [CarPosition] -> BotM IO ()
updateCars tick carPositions = do
    b <- get
    let newDatas = map (\cp -> let n   = carIdName $ CPM.carId cp
                                   ocp = cdPosition $ fromJust $ Map.lookup n $ bCars b
                               in (n, newPieceData ocp cp ((pieces $ bTrack b) !! (pieceIndex ocp)) ((lanes $ bTrack b) !! (endLaneIndex $ lane $ piecePosition cp)))
                       ) carPositions
        newCps = Map.mapWithKey (\n pds -> fromMaybe pds (lookup n newDatas >>= (Just . updateCarPieceData pds))) $ bCarPieceData b
        updatePieces = map fst $ filter (\(_,(_,n)) -> length n == 1) $ zip (Map.toList $ bCarPieceData b) (Map.toList newCps)

    -- forM_ updatePieces (\(_, cpd) ->
    --     when ((length cpd) > 0) $ do
    --         let avg = (pdSpeed $ head cpd) - 0.6 -- (sum $ map pdSpeed cpd) / (fromIntegral $ length cpd)
    --             pix = pdPieceIndex $ head cpd

    --         modify (\b -> b { bPieceKnowledge = over (element pix) (\pk -> if speed pk >= avg then trace "Did not update" pk else trace "Updating speed due to avg" $ pk { speed = max (speed pk) ((pdSpeed $ head cpd) - 0.6) } ) (bPieceKnowledge b) })
    --     )

    modify (\b -> b { bCars = foldl (\cs cp -> adjust (updateCarData (bTrack b) cp) (carIdName $ CPM.carId cp) cs) (bCars b) carPositions
                    , bTick = tick
                    , bCarPieceData = newCps
                    } )

switchLane :: Side -> Int -> BotM IO ActionChoice
switchLane s tick = do
    liftIO $ print ("TICK " ++ (show tick) ++ " Sending switch.. " ++ (show s))
    choose $ SwitchChoice s

adjustThrottle :: PiecePosition -> Int -> BotM IO ActionChoice
adjustThrottle piecePos tick = do
    brain <- get
    let pieceIdx = pieceIndex piecePos
        speed = knownSpeed pieceIdx brain
        ps = pieces $ bTrack brain
        thisPiece = ps !! pieceIdx
        thisIsBend = isBend thisPiece
        myLane = (lanes $ bTrack brain) !! (endLaneIndex $ lane $ piecePos)
        pix = (until (\p -> not $ (ps !! pieceIdx) == (ps !! p)) (\pi -> let pi' = pi + 1 in if pi' == length ps then 0 else pi') pieceIdx)
        slowPointIdx = fromJust $ nextBendIndex ps pix
        sp = ps !! slowPointIdx
        slowSeg = fullBend slowPointIdx ps
        slowSpeed = knownSpeed slowPointIdx brain
        distanceToSlow = distanceTo' piecePos myLane slowPointIdx ps
        slowBrakeDist = calculateBrakeDistance' brain slowSpeed (bCurrentSpeed brain)

    when ((bCurrentSpeed brain) < 0) $ do
        liftIO $ print ((pieces $ bTrack brain) !! ((pieceIndex $ bMyPosition brain) - 1))
        liftIO $ print (bMyPosition brain)

    -- liftIO $ print ("DIST TO SLOW...")
    -- liftIO $ print $ show distanceToSlow

    -- liftIO $ print $ concat ["P ", show pieceIdx, " B ", show ((fromJust bendIdx) + pieceIdx), " mid ", show bendDist', " start ", show bendDist]
    -- liftIO $ print $ concat [show $ bCurrentSpeed brain, ",", show myAngle]
    -- if ((speed < (min 1.0 $ bCurrentSpeed brain)) || ((distanceToSlow <= slowBrakeDist) && (bCurrentSpeed brain) > slowSpeed))
    if (distanceToSlow <= slowBrakeDist) || (bCurrentSpeed brain) > speed
      then do
        liftIO $ print ("TICK " ++ (show tick) ++ ", " ++ (show pieceIdx) ++ " Braking " ++ (show $ bCurrentSpeed brain) ++ "/" ++ (show speed) ++ " (" ++ (show $ CarPositionsModel.angle $ bMyCarPosition brain) ++ ") :: " ++ (show distanceToSlow) ++ "/" ++ (show slowPointIdx) ++ "/" ++ (show slowBrakeDist) ++ "/" ++ (show slowSpeed))
        choose BrakeChoice
      else do
        -- spd <- if thisIsBend then regulateSpeed thisPiece else return (if bCurrentSpeed brain < 1 then clampThrottle speed else adjustThrottleForTurbo brain speed)
        spd <- liftM (min 1) $ regulateSpeed thisPiece
        liftIO $ print ("TICK " ++ (show tick) ++ ", " ++ (show pieceIdx) ++ " Speed " ++ (show $ bCurrentSpeed brain) ++ "/" ++ (show spd) ++ "/" ++ (show speed) ++ " (" ++ (show $ CarPositionsModel.angle $ bMyCarPosition brain) ++ ") :: " ++ (show distanceToSlow) ++ "/" ++ (show slowPointIdx) ++ "/" ++ (show slowBrakeDist) ++ "/" ++ (show slowSpeed))
        modify (\b -> b { bCurrentThrottle = spd })
        choose $ AccelerateChoice spd

regulateSpeed :: Piece -> BotM IO Speed
regulateSpeed p = do
        b <- get
        let maxAngle = (sqrt (fromIntegral $ fromMaybe 200 $ radius p)) * 3.8
            err = (maxAngle - (abs $ CPM.angle $ bMyCarPosition b)) / 30
            perr = if (bRegulateError b) == 0 then err else bRegulateError b
            integral = min 0 ((bRegulateIntegral b) + err)
            derivative = err - perr
            kp = 1
            ki = 0
            kdp = 10
            kdn = 6
            kd = if derivative > 0 then kdp else kdn
            output = 0.5 + kp * err + ki * integral + derivative * kd

        modify (\b -> b { bRegulateError = err, bRegulateIntegral = integral })

        return $ clampThrottle $ adjustThrottleForTurbo b (output)

slipSpeed :: Float -> Float
slipSpeed s | s < 50 = 0.6
            | otherwise = 0

updateBrain :: String -> [CarPosition] -> Brain -> Brain
updateBrain name cps b = newBrain
                      { bSwitchPending = if bSwitchPending b && (not switchingLane) && (not passedSwitch) then True else False
                      , bSwitchingLane = switchingLane
                      , bMyPosition = myPiece
                      , bMyCarPosition = myCar
                      , bCurrentSpeed = if newSpeed < 0 then oldSpeed else newSpeed -- due to inaccurate calculation, this sometimes goes nuts
                      , bRegulateIntegral = if outOfBend then 0 else bRegulateIntegral b
                      , bRegulateError = if outOfBend then 0 else bRegulateError b
                      , bMass = newMass
                      , bDrag = newDrag
                      }
    where
        myCar = fromJust $ findCar name cps
        myPiece = piecePosition myCar
        myLane = lane myPiece
        switchingLane = startLaneIndex myLane /= endLaneIndex myLane
        positionDiff = (inPieceDistance myPiece) - (inPieceDistance $ bMyPosition b)
        myLaneOffset = distanceFromCenter ((lanes $ bTrack b) !! endLaneIndex myLane)
        ps = pieces $ bTrack b
        fromPiece = ps !! (pieceIndex $ bMyPosition b)
        toPiece = ps !! (pieceIndex myPiece)
        passedSwitch = (isSwitch fromPiece) && (not $ isSwitch toPiece)
        outOfBend = (not $ isBend toPiece) && (isBend fromPiece)
        newBrain = analyze'' b
        oldSpeed = bCurrentSpeed b
        newSpeed = if positionDiff < 0 then (positionDiff + (pieceLen myLaneOffset $ (pieces $ bTrack b) !! (pieceIndex $ bMyPosition b))) / 10 else positionDiff / 10
        thisKnowledge = (bPieceKnowledge b) !! (pieceIndex $ bMyPosition b)
        pieceSpeed = if isBend fromPiece then max (bCurrentSpeed b) (bCurrentThrottle b) else 1.0
        analyze'' b = if (pieceIndex myPiece) /= (pieceIndex $ bMyPosition b)
                        then b { bPieceKnowledge = map (\pk -> if pkDifficulty pk <= pkDifficulty thisKnowledge then  pk else pk) $ map clampByAngle $ bPieceKnowledge b }
                        else b
        clampByAngle = if abs (CPM.angle myCar) < 58
                         then id
                         else (\pk -> if pkDifficulty pk > pkDifficulty thisKnowledge && maxSpeed pk > (bCurrentSpeed b) then pk { maxSpeed = bCurrentSpeed b } else pk)
        myData = Map.lookup name (bCarPieceData b)
        goodSpeeds = myData >>= return . map ((* 10) . pdSpeed) . take 3 . filter ((> 0) . pdSpeed)
        newDrag = bDrag b `mplus` (goodSpeeds >>= (\ss -> if length ss < 3 then Nothing else Just $ calcDrag ss))
        newMass = bMass b `mplus` (goodSpeeds >>= (\ss -> if length ss < 3 then Nothing else Just $ calcMass ss (calcDrag ss)))
        calcDrag [a, b, c] = let d = (a - (b - a)) / (a * a) in trace ("NEW DRAG: " ++ (show [a,b,c,d])) d
        calcDrag _ = 0.02
        calcMass [a, b, c] drag = let m = 1 / ((log ((c - (1 / drag)) / (b - (1 / drag)))) / (0 - drag)) in trace ("NEW MASS: " ++ (show [a,b,c,drag,m])) m
        calcMass _ _ = 0.02

decodeMessage :: (String, Value, Maybe Int) -> Result ServerMessage
decodeMessage (msgType, msgData, mTick)
  | msgType == "join" = Success Main.Join
  | msgType == "gameInit" = GameInit <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData) <*> pure (fromMaybe (-1) mTick)
  | msgType == "gameStart" = Success GameStart
  | msgType == "lapFinished" = Success LapFinished
  | msgType == "crash" = Crash <$> pure (fromMaybe (-1) mTick) <*> (fromJSON msgData)
  | msgType == "spawn" = Spawn <$> pure (fromMaybe (-1) mTick) <*> (fromJSON msgData)
  | msgType == "gameEnd" = Success GameEnd
  | msgType == "turboAvailable" = TurboAvailable <$> (fromJSON msgData)
  | msgType == "turboEnd" = TurboEnd <$> (fromJSON msgData)
  | msgType == "dnf" = DNF <$> (fromJSON msgData)
  | otherwise = Success $ Unknown (msgType ++ " " ++ (show msgData))

instance FromJSON a => FromJSON (String, a, Maybe Int) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    maybeTick <- v .:? "gameTick"
    return (msgType, msgData, maybeTick)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)
